import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  // Doughnut
  public doughnutChartLabels:string[] = ['Gilsenior', 'Bruno', 'João'];
  public doughnutChartData:number[] = [30, 450, 300];
  public doughnutChartType:string = 'doughnut';


  constructor(public navCtrl: NavController) {

  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }

}
